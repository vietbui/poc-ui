const mutationTypes = {
  SHOW: 'SHOW',
  CLEAR: 'CLEAR'
}

const state = {
  toast: {}
}

const getters = {
}

const actions = {
  showInfo: ({ commit }, message) => {
    commit(mutationTypes.SHOW, { color: 'info', message })
  },
  showError: ({ commit }, message) => {
    commit(mutationTypes.SHOW, { color: 'error', message })
  },
  clear: ({ commit }) => {
    commit(mutationTypes.CLEAR)
  }
}

const mutations = {
  [mutationTypes.SHOW] (state, { color, message }) {
    state.toast = {
      show: true,
      color,
      message
    }
  },
  [mutationTypes.CLEAR] (state) {
    state.toast = {}
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
