const mutationTypes = {
  TOGGLE_DRAWER: 'TOGGLE_DRAWER',
  SET_DRAWER: 'SET_DRAWER'
}

const state = {
  drawer: true
}

const getters = {
}

const actions = {
  toggleDrawer: ({ commit }) => {
    commit(mutationTypes.TOGGLE_DRAWER)
  },
  setDrawer: ({ commit }, drawer) => {
    commit(mutationTypes.SET_DRAWER, drawer)
  }
}

const mutations = {
  [mutationTypes.TOGGLE_DRAWER] (state) {
    state.drawer = !state.drawer
  },
  [mutationTypes.SET_DRAWER] (state, drawer) {
    state.drawer = drawer
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
