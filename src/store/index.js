import Vue from 'vue'
import Vuex from 'vuex'
import createPersistedState from 'vuex-persistedstate'

import snackbar from './modules/snackbar'
import home from './modules/home'

Vue.use(Vuex)

const debug = process.env.NODE_ENV !== 'production'

export default new Vuex.Store({
  modules: {
    snackbar,
    home
  },
  strict: debug,
  plugins: [createPersistedState({
    key: 'vv-admin',
    paths: [
    ]
  })]
})
