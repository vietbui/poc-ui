import Vue from 'vue'
import Router from 'vue-router'

import Home from '@/layouts/home/Home'
import Dashboard from '@/pages/Dashboard'
import Devices from '@/pages/Devices'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      component: Home,
      redirect: { name: 'dashboard' },
      children: [
        {
          path: 'dashboard',
          name: 'dashboard',
          component: Dashboard
        },
        {
          path: 'devices',
          name: 'devices',
          component: Devices
        }
      ]
    }
  ]
})
